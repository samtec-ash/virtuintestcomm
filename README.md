
# ![Virtuin Logo](https://s3.amazonaws.com/virtuin-static-images/64x64.png) VirtuinTestComm  

## Overview

VirtuinTestComm. provides utilities for broadcasting, direct, etc communication over RabbitMQ.  
The following classes are provided:

* __VirtuinTestPublisher__: Used by active running test to update status and results to subscribers.

* __VirtuinTestSubscriber__: Used by any process wanting to receive test status/result updates.

* __VirtuinTestViewClient__: Used by test view (e.g. web-server) to send requests to test view server.

* __VirtuinTestViewServer__: Used primarily by active test to listen for view requests.  


### Build

```bash
yarn run prepublish
```

### Installation

```bash
yarn add @samtec-ash/virtuintestcomm
```

### Examples

##### VirtuinTestPublisher

```javascript
import {
  VirtuinTestPublisher
} from '@samtec-hub/virtuintestcomm';

publisher = new VirtuinTestPublisher(
  'DebugStation',
  'DebugTest',
  'DebugUUID'
);

await publisher.open('localhost');
publisher.updateStatus(
  state='STARTING',
  progress=0
);
await publisher.publish(
  message='Just warming up.',
  error=undefined,
  customDict={a=1}
);
publisher.updateStatus(
  state='FINISHED',
  progress=100
);
await publisher.publish(
  message='All done.',
  error=undefined,
  results=[{type='scalar', name='Temp', unit='C', value='28'}],
  customDict={a=2}
);
await publisher.close();
```
##### VirtuinTestSubscriber

```javascript
import {
  VirtuinTestSubscriber
} from '@samtec-hub/virtuintestcomm';

subscriber = new VirtuinTestSubscriber('DebugStation');
await subscriber.open('localhost');
let subscribeTag;
const subscribeCB = async (err, data) => {
  if (err) {
    console.log(`Received error ${err}`);
  } else if (data) {
    if (data.state === 'STARTED') {
      console.log('Test started');
    } else if (data.progress === 100 || data.state === 'FINISHED') {
      console.log('Test finished');
      await subscriber.unsubscribe(subscribeTag);
      await subscriber.close();
    }
  }  
};
subscribeTag = await subscriber.subscribe(subscribeCB);
```

##### VirtuinTestViewServer

```javascript
import {
  VirtuinTestViewServer
} from '@samtec-hub/virtuintestcomm';

viewServer = new VirtuinTestViewServer('DebugStation', 'DebugTest');
await viewServer.open('localhost');

let listenTag;
const listenCB = async (err, data) => {
  if (err) {
    console.log(`Received error ${err}`);
  } else if (data) {
    if (typeof data.operation === 'string') {
      switch(data.operation.toUpperCase()) {
        case 'FOO': {
          console.log('Doing FOO');
          return { success: true, operation: 'FOO' };
        }
        case 'BAR': {
          console.log('Doing BAR');
          return { success: true, operation: 'BAR' };
        }
        case 'EXIT': {          
          setTimeout(() => { viewServer.close(); }, 500);
          return { success: true, operation: 'EXIT' };
        }
        default: {
          console.log('Operation unknown');
          return { success: false };
        }
      }
    }
  }  
};
listenTag = await viewServer.listen(listenCB);
```

##### VirtuinTestViewClient

```javascript
import {
  VirtuinTestViewClient
} from '@samtec-hub/virtuintestcomm';

viewClient = new VirtuinTestViewClient('DebugStation', 'DebugTest');
await viewClient.open('localhost');
viewClient.write({ operation: 'FOO' });
viewClient.close();
```

### API

[API HTML](docs/index.html)  
[API MD](docs/api.md)

## Publishing

New versions are published to [npmjs.com](https://www.npmjs.com).
BitBucket Pipelines is used to build, test, stage, & deploy. Refer to the [pipeline configuration](https://bitbucket.org/samteccmd/virtuintestcomm/src/master/bitbucket-pipelines.yml).

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/samteccmd/virtuintestcomm/commits/).

## Authors

* **Adam Page**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
