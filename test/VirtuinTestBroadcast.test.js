import { VirtuinTestPublisher, VirtuinTestSubscriber } from '../src/index';

describe('VirtuinBroadcast communication', () => {
  const pub = new VirtuinTestPublisher('FakeStation', 'FakeTest', '12345');
  const sub = new VirtuinTestSubscriber('FakeStation');
  beforeAll(async () => {
    await pub.open('localhost');
    await sub.open('localhost');
  });

  it('should do something', async () => {
    expect.assertions(1);
    setTimeout(async () => {
      pub.version = '0.9.3';
      pub.updateStatus('STARTED', 0, undefined);
      await pub.publish('Message 1');
    }, 10);

    const rst = await new Promise((resolve) => {
      setTimeout(async () => {
        await sub.subscribe((err, lclRst) => {
          resolve(lclRst);
        });
      }, 1);
    });
    expect(rst).toMatchObject({
      version: '0.9.3',
      status: {
        testUUID: '12345',
        testName: 'FakeTest',
        state: 'STARTED',
        progress: 0,
        message: 'Message 1'
      }
    });
  });

  afterAll(async () => {
    await pub.close();
    await sub.close();
  });
});
