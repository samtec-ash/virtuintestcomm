import { tryParseJSON, openBroker } from './Shared';

/**
 * @classdesc Used to subscribe to test's status updates over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
class VirtuinTestSubscriber {
  constructor(stationName) {
    this.retryDelay = 100;
    this.exName = `${stationName}`;
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
  }

  /**
   * Open connection to RabbitMQ
   * @async
   * @param {string} brokerAddress - Address of broker
   * @throws {object} Error object
   * @return {void}
   */
  open = async (brokerAddress = 'localhost', timeoutms = 5000) => {
    const numAttempts = Math.max(1, Math.ceil(timeoutms / this.retryDelay));
    this.conn = await openBroker(brokerAddress, numAttempts, this.retryDelay);
    this.ch = await this.conn.createChannel();
    await this.ch.assertExchange(this.exName, 'fanout', { durable: false });
    this.isOpen = true;
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (!this.isOpen) {
      return;
    }
    await this.conn.close();
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
  }

  /**
   * Start subscribing to test status updates
   * @param {function} consumeCB - Called when status updates received
   * (error: ?Error, jsonData: Obj) => void
   * @return {string} consumerTag
   * @throws Error object
   */
  subscribe = async (consumeCB) => {
    if (!this.isOpen) {
      return undefined;
    }
    const qName = '';
    const q = await this.ch.assertQueue(qName, { exclusive: true });
    this.ch.bindQueue(q.queue, this.exName, '');
    const consumeID = await this.ch.consume(q.queue, (packet) => {
      const jsonData = tryParseJSON(packet.content) || {};
      consumeCB(null, jsonData);
    }, { noAck: true });
    return consumeID.consumerTag;
  }

  /**
   * Stop subscribing to test status updates
   * @param {string} consumerTag - Queue tag returned from subscribing
   * @throws Error object
   */
  unsubscribe = async (consumerTag) => {
    if (!this.isOpen) {
      return undefined;
    }
    return this.ch.cancel(consumerTag);
  }
}

export default VirtuinTestSubscriber;
