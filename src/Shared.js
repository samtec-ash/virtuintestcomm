import * as amqp from 'amqplib';

/**
 * Open connection to RabbitMQ broker
 * @async
 * @param {string} brokerAddress - Address of broker
 * @param {number} maxAttempts - Number of attempts
 * @param {number} delayms - Millisecond delay
 * @throws {object} Error object
 * @return {void}
 */
async function openBroker(brokerAddress, maxAttempts, delayms) {
  const openTimeout = async (host, delay) => (
    new Promise((resolve) => {
      setTimeout(async () => {
        try {
          const conn = await amqp.connect(host);
          resolve(conn);
        } catch (err) {
          resolve(null);
        }
      }, delay);
    })
  );
  const hostAddress = `amqp://${brokerAddress}`;
  let conn = null;
  for (let attempts = 0; attempts < maxAttempts; attempts += 1) {
    // eslint-disable-next-line no-await-in-loop
    conn = await openTimeout(hostAddress, attempts > 0 ? delayms : 0);
    if (conn != null) {
      return conn;
    }
  }
  throw new Error('Failed connecting to broker. Ensure broker is running and address is correct');
}

/**
 * Parses json string into object. Doesn't throw with invalid json string.
 * @param  {string} jsonString - json string
 * @return {object|undefined} - json object or undefined if invalid json
 */
function tryParseJSON(jsonString) {
  try {
    const data = JSON.parse(jsonString);
    return (data && typeof data === 'object') ? data : undefined;
  } catch (err) {
    return undefined;
  }
}

// eslint-disable-next-line import/prefer-default-export
export { tryParseJSON, openBroker };
