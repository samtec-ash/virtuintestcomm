import { tryParseJSON, openBroker } from './Shared';

/**
 * @classdesc Used to listen for view requests over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
class VirtuinTestViewServer {
  constructor(stationName, testName) {
    this.retryDelay = 100;
    this.stationName = stationName;
    this.exName = `${stationName}_view`;
    this.qName = testName;
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
    this.isListening = false;
  }

  /**
   * Open connection to RabbitMQ
   * @async
   * @param {string} brokerAddress - Address of broker
   * @throws {object} Error object
   * @return {void}
   */
  open = async (brokerAddress = 'localhost', timeoutms = 5000) => {
    const numAttempts = Math.max(1, Math.ceil(timeoutms / this.retryDelay));
    this.conn = await openBroker(brokerAddress, numAttempts, this.retryDelay);
    this.ch = await this.conn.createChannel();
    this.ch.assertExchange(this.exName, 'direct', { durable: false });
    const q = await this.ch.assertQueue(this.qName, { exclusive: true });
    this.ch.bindQueue(q.queue, this.exName, q.queue);
    this.ch.prefetch(1);
    this.isOpen = true;
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (!this.isOpen) {
      return;
    }
    await this.conn.close();
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
    this.isListening = false;
  }

  /**
   * Listen for requests from view clients.
   * @param {function} consumeCB - callback function called whenever request is received
   * (error: ?Error, jsonData: Obj) => Obj
   * @return {string} consumerTag - Tag identifier used to explicitly stop listening
   * @throws Error object
  */
  listen = async (consumeCB) => {
    if (!this.isOpen || this.isListening) {
      return;
    }

    const consumeID = await this.ch.consume(this.qName, async (packet) => {
      try {
        const jsonData = tryParseJSON(packet.content) || {};
        // eslint-disable-next-line no-unused-vars
        const rstDict = await consumeCB(null, jsonData);
        // TODO: May want to reply with acknowledge packet when complete
        // const replyQ = packet.properties.replyTo;
        // const payloadBuffer = new Buffer(JSON.stringify(payload));
        // const queueOptions = { correlationId: packet.properties.correlationId };
        // this.ch.sendToQueue(replyQ, payloadBuffer, queueOptions);
        this.ch.ack(packet);
      } catch (err) {
        consumeCB(new Error(`Failed consuming packet ${err.message}`), null);
      }
    });
    this.isListening = true;
    return consumeID.consumerTag;
  }
}

export default VirtuinTestViewServer;
