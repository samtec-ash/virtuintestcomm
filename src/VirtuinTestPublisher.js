import { openBroker } from './Shared';

/**
 * @classdesc Used by active tests to publish status updates over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
class VirtuinTestPublisher {
  constructor(stationName, testName, testUUID = undefined) {
    this.version = '0.9.8';
    this.exName = `${stationName}`;
    this.testName = testName;
    this.testUUID = testUUID !== undefined ? testUUID : `${testName}_${(new Date()).toUTCString()}`;
    this.state = 'STARTED';
    this.progress = 0;
    this.passed = undefined;
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
    this.retryDelay = 100;
  }

  /**
   * Open connection to RabbitMQ
   * @async
   * @param {string} brokerAddress - Address of broker
   * @throws {object} Error object
   * @return {void}
   */
  open = async (brokerAddress = 'localhost', timeoutms = 5000) => {
    const numAttempts = Math.max(1, Math.ceil(timeoutms / this.retryDelay));
    this.conn = await openBroker(brokerAddress, numAttempts, this.retryDelay);
    this.ch = await this.conn.createChannel();
    await this.ch.assertExchange(this.exName, 'fanout', { durable: false });
    this.isOpen = true;
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (!this.isOpen) {
      return;
    }
    await this.conn.close();
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
  }

  /**
   * Create base payload object to be published
   * @param {string} message - General status message
   * @param {string} error - Error message
   * @return {object} - Payload object
   */
  _getBasePayload = (message = '', error = undefined) => (
    {
      version: this.version,
      status: {
        testUUID: this.testUUID,
        testName: this.testName,
        state: this.state,
        passed: this.passed,
        progress: this.progress,
        error,
        message
      }
    }
  )

  /**
   * Publishes payload object
   * @param {object} data - Payload object
   * @return {void}
   */
  _publishData = async (data) => {
    const qName = ''; // Push to all queues in exchange
    await this.ch.publish(this.exName, qName, Buffer.from(JSON.stringify(data)));
  }

  /**
   * Update test status payload
   * @param {string} state - State of test (STARTING, ..., FINISHED)
   * @param {number} progress - Progress of test [0-100]
   * @param {boolean} passed - If test passed/failed
   * @return {void}
   */
  updateStatus = (state = undefined, progress = undefined, passed = undefined) => {
    this.state = state || this.state;
    this.progress = progress || this.progress;
    this.passed = passed !== undefined ? passed : this.passed;
  }

  /**
   * Publish test status payload
   * @async
   * @param {string} message - General status message
   * @param {string} error - Error message
   * @param {object} results - Serializable Database results object
   * @param {object} customDict - Serializable object to merge into status payload
   * @return {void}
   * @throws {object} Error object
   */
  publish = async (message = '', error = undefined, results = undefined, customDict = undefined) => {
    let data = this._getBasePayload(message, error);
    if (results) {
      data.results = results;
    }
    if (customDict) {
      data = Object.assign(data, customDict);
    }
    return this._publishData(data);
  }
}

export default VirtuinTestPublisher;
