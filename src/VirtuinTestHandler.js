// @flow

import * as amqp from 'amqplib';

/**
 * @classdesc Used to handle test start/stop/status requests over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.8
 */
class VirtuinTestHandler {
  stationName: string;
  testServerQName: string;
  conn: ?Object;
  ch: ?Object;
  isOpen: boolean;
  isListening: boolean;

  constructor(stationName: string) {
    this.stationName = stationName;
    this.testServerQName = `${stationName}`;
    this.conn = null;
    this.ch = null;
    this.isOpen = false;
    this.isListening = false;
  }

  /**
   * Open connection to RabbitMQ
   * @async
   * @param {string} brokerAddress - Address of broker
   * @throws {object} Error object
   * @return {void}
   */
  open = async (brokerAddress: string = 'localhost') => {
    try {
      const host = `amqp://${brokerAddress}`;
      const conn: Object = await amqp.connect(host);
      const ch: Object = await conn.createChannel();
      await ch.assertQueue(this.testServerQName, { durable: true });
      ch.prefetch(1);
      this.conn = conn;
      this.ch = ch;
      this.isOpen = true;
      return true;
    } catch (err) {
      this.isOpen = false;
      throw err;
    }
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (this.isOpen && this.conn != null) {
      await this.conn.close();
      this.conn = undefined;
      this.ch = undefined;
      this.isListening = false;
      this.isOpen = false;
    }
  }

  /**
   * Start listening for requests
   * @param {function} consumeCB - callback function called when request is received
   * (error: ?Error, jsonData: Obj) => Obj
   * @return {string} consumerTag - Tag identifier used to explicitly stop listening
   * @throws Error object
  */
  listen = async (consumeCB: (error: Error | null, value: Object | null) => void) => {
    if (!this.isOpen || this.ch == null || this.isListening) {
      throw Error('Connection not open or already listening');
    }

    const consumeID = await this.ch.consume(this.testServerQName, async (packet: Object) => {
      try {
        const jsonData = JSON.parse(packet.content);
        const cmd = jsonData.command;
        const rstDict = await consumeCB(null, jsonData);
        const replyQ = packet.properties.replyTo;
        const payload = {
          id: packet.properties.correlationId,
          command: cmd,
          error: undefined,
          success: true
        };
        Object.assign(payload, rstDict);
        const payloadBuffer = Buffer.from(JSON.stringify(payload));
        const queueOptions = { correlationId: packet.properties.correlationId };
        if (this.ch != null) {
          const { ch } = this;
          ch.sendToQueue(replyQ, payloadBuffer, queueOptions);
          ch.ack(packet);
        }
      } catch (err) {
        consumeCB(new Error(`Failed consuming packet ${err.message}`), null);
      }
    });
    this.isListening = true;
    return consumeID.consumerTag;
  }
}

export default VirtuinTestHandler;
