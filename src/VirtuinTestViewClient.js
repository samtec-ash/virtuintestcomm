import { openBroker } from './Shared';

/**
 * @classdesc Used to perform requests to view server over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
class VirtuinTestViewClient {
  constructor(stationName, testName) {
    this.stationName = stationName;
    this.exName = `${stationName}_view`;
    this.qName = testName;
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
    this.retryDelay = 100;
  }

  /**
   * Open connection to RabbitMQ
   * @async
   * @param {string} brokerAddress - Address of broker
   * @throws {object} Error object
   * @return {void}
   */
  open = async (brokerAddress = 'localhost', timeoutms = 5000) => {
    const numAttempts = Math.max(1, Math.ceil(timeoutms / this.retryDelay));
    this.conn = await openBroker(brokerAddress, numAttempts, this.retryDelay);
    this.ch = await this.conn.createChannel();
    this.ch.assertExchange(this.exName, 'direct', { durable: false });
    this.isOpen = true;
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (!this.isOpen) {
      return;
    }
    await this.conn.close();
    this.conn = undefined;
    this.ch = undefined;
    this.isOpen = false;
  }

  /**
   * Write data to view server.
   * @param {object} data - Serializable object to send to view server.
   * @param {string} qName - Name of queue to publish to.
   * @return {void}
   * @throws Error object
  */
  write = (data, qName = undefined) => {
    if (!this.isOpen) {
      return;
    }
    this.ch.publish(this.exName, qName || this.qName, Buffer.from(data));
  }
}

export default VirtuinTestViewClient;
