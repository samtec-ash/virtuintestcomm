// @flow

import { tryParseJSON, openBroker } from './Shared';

/**
 * @classdesc Used to perform test requests (start/stop/status) over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
class VirtuinTestDispatcher {
  stationName: string;
  testServerQName: string;
  conn: ?Object;
  ch: ?Object;
  subscriptions: Object;
  isOpen: boolean;
  retryDelay: number;

  constructor(stationName: string) {
    this.stationName = stationName;
    this.testServerQName = `${stationName}`;
    this.conn = undefined;
    this.ch = undefined;
    this.subscriptions = {};
    this.isOpen = false;
    this.retryDelay = 100;
  }

  /**
   * Create unique identifier.
   * @return {string} - unique identifier
   */
  _generateUUID = (): string => (
    Math.random().toString() +
    Math.random().toString() +
    Math.random().toString()
  );

  /**
  * Open connection to RabbitMQ
  * @async
  * @param {string} brokerAddress - Address of broker
  * @throws {object} Error object
  * @return {void}
  */
  open = async (brokerAddress: string = 'localhost', timeoutms: number = 2000) => {
    const numAttempts = Math.max(1, Math.ceil(timeoutms / this.retryDelay));
    const conn = await openBroker(brokerAddress, numAttempts, this.retryDelay);
    const ch :Object = await conn.createChannel();
    ch.assertQueue(this.testServerQName, { durable: true });
    this.conn = conn;
    this.ch = ch;
    this.isOpen = true;
  }

  /**
   * Close connection to RabbitMQ
   * @async
   * @throws {object} Error object
   * @return {void}
   */
  close = async () => {
    if (!this.isOpen) {
      return;
    }
    const consumerTags = Object.keys(this.subscriptions);
    consumerTags.forEach(async (consumerTag) => {
      await this.unsubscribe(consumerTag);
    });
    if (this.conn != null) {
      await this.conn.close();
    }
    this.conn = undefined;
    this.ch = undefined;
    this.subscriptions = {};
    this.isOpen = false;
  }

  /**
   * Start requesting & subscribing to test handler status updates
   * @async
   * @param {function} consumeCB - callback function (error: ?Error, jsonData: Obj) => void
   * @param {number} msTimeInterval - Time interval between querying status in milliseconds.
   * @return {string} consumerTag
   * @throws Error object
   */
  subscribe = async (consumeCB: (error: ?Error, value: ?Object) => void,
  msTimeInterval: number = 5000) => {
    if (!this.isOpen || this.ch == null) {
      return undefined;
    }
    const { ch } = this;

    const corrID = this._generateUUID();
    const replyQ = await ch.assertQueue('', { durable: false, exclusive: true });
    const statusInterval = setInterval(() => {
      const queueOptions = { correlationId: corrID, replyTo: replyQ.queue };
      const statusRequest = { command: 'STATUS' };
      const statusBuffer = Buffer.from(JSON.stringify(statusRequest));
      ch.sendToQueue(this.testServerQName, statusBuffer, queueOptions);
    }, msTimeInterval);

    const consumeID = await ch.consume(replyQ.queue, (packet) => {
      const jsonData = tryParseJSON(packet.content) || {};
      consumeCB(null, jsonData);
    });
    this.subscriptions[consumeID.consumerTag] = { statusInterval };
    return consumeID.consumerTag;
  }

  /**
   * Stop requesting & subscribing to test handler status updates
   * @async
   * @param {string} consumerTag - Queue tag returned from subscribing
   * @throws Error object
   */
  unsubscribe = async (consumerTag: string) => {
    if (!this.isOpen || this.ch == null) {
      return;
    }
    const { ch } = this;

    if (consumerTag in this.subscriptions) {
      clearInterval(this.subscriptions[consumerTag].statusInterval);
      ch.cancel(consumerTag);
      delete this.subscriptions[consumerTag];
    }
  }

  /**
   * Sends generic payload object to test handler
   * @async
   * @param {object} payload - Serializable payload object to send
   * @param {string} corrID - Optionally set identifier for reply queue
   * @returns {Promise}
   * @throws Error object
   */
  _sendCommand = async (payload: Object, corrID: ?string = undefined) => {
    if (this.ch == null) {
      throw new Error('RabbitMQ channel is not open.');
    }
    const uuid = corrID || this._generateUUID();
    const { ch } = this;
    const payloadBuffer = Buffer.from(JSON.stringify(payload));
    const replyQ = await ch.assertQueue('', { durable: false, exclusive: true });
    const queueOptions = { correlationId: uuid, replyTo: replyQ.queue };
    ch.sendToQueue(this.testServerQName, payloadBuffer, queueOptions);

    return Promise.race([
      new Promise((resolve, reject) => {
        setTimeout(reject, 10000, new Error('Timeout occurred before receiving response from test server.'));
      }),
      new Promise((resolve) => {
        ch.consume(replyQ.queue, (packet) => {
          if (packet.properties.correlationId === uuid) {
            ch.cancel(packet.fields.consumerTag);
            ch.deleteQueue(replyQ.queue);
            resolve(packet.content);
          }
        }, { noAck: true });
      })
    ]);
  }

  /**
   * Request to bring up test environment
   * @async
   * @param {object} collection - Serializable test collection object
   * @returns {object} Result object
   * @throws Error object
   */
  up = async (collection: Object, fullReload: boolean = false) => {
    if (!this.isOpen) {
      throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');
    }
    try {
      const runConfigs = {
        collection,
        fullReload,
        command: 'UP'
      };
      const response = await this._sendCommand(runConfigs, undefined);
      const rst = tryParseJSON(response);
      return rst;
    } catch (err) {
      throw new Error(`Failed bringing up test environment. Received error: ${err}.`);
    }
  }

  /**
   * Request to bring up test environment
   * @async
   * @param {object} collection - Serializable test collection object
   * @returns {object} Result object
   * @throws Error object
   */
  down = async (rm: boolean = false) => {
    if (!this.isOpen) {
      throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');
    }
    try {
      const runConfigs = {
        rm,
        command: 'DOWN'
      };
      const response = await this._sendCommand(runConfigs, undefined);
      const rst = tryParseJSON(response);
      return rst;
    } catch (err) {
      throw new Error(`Failed bringing down test environment. Received error: ${err}.`);
    }
  }

  /**
   * Request a test to start with given configs
   * @async
   * @param {object} testConfigs - Serializable test configs object to send
   * @returns {object} Result object
   * @throws Error object
   */
  startTest = async (testConfigs: Object) => {
    if (!this.isOpen) {
      throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');
    }
    try {
      const testUUID = this._generateUUID();
      const runConfigs = {
        ...testConfigs,
        command: 'START',
        testUUID
      };
      const response = await this._sendCommand(runConfigs, testUUID);
      const rst = tryParseJSON(response);
      return { ...rst, testUUID };
    } catch (err) {
      throw new Error(`Failed starting test. Received error: ${err}.`);
    }
  }

  /**
   * Request a test to be killed if running
   * @async
   * @param {object} testConfigs - Test configs (need testName and testUUID at minimum)
   * @returns {object} result object
   * @throws Error object
   */
  stopTest = async (testConfigs: Object) => {
    if (!this.isOpen) {
      throw Error('Failed stopping test. Virtuin Test Dispatcher is not open.');
    }
    try {
      const runConfigs = {
        ...testConfigs,
        command: 'STOP'
      };
      const response = await this._sendCommand(runConfigs, undefined);
      const rst = tryParseJSON(response);
      return rst;
    } catch (err) {
      throw new Error(`Failed stopping test. Received error: ${err}.`);
    }
  }

  /**
   * Request to clear finished test
   * @async
   * @param {object} testConfigs - Test configs (logs boolean?)
   * @returns {object} result object
   * @throws Error object
   */
  clearTest = async (testConfigs: Object) => {
    if (!this.isOpen) {
      throw Error('Failed clearing test. Virtuin Test Dispatcher is not open.');
    }
    try {
      const runConfigs = Object.assign({}, testConfigs, { command: 'CLEAR' });
      const response = await this._sendCommand(runConfigs, undefined);
      const rst = tryParseJSON(response);
      return rst;
    } catch (err) {
      throw new Error(`Failed clearing test. Received error: ${err}.`);
    }
  }

  /**
   * Request status
   * @async
   * @returns {object} result object
   * @throws Error object
   */
  getStatus = async () => {
    if (!this.isOpen) {
      throw Error('Failed getting status. Virtuin Test Dispatcher is not open.');
    }
    try {
      const packet = Object.assign({}, { command: 'STATUS' });
      const response = await this._sendCommand(packet, undefined);
      const rst = tryParseJSON(response);
      return rst;
    } catch (err) {
      throw new Error(`Failed getting status. Received error: ${err}.`);
    }
  }
}


export default VirtuinTestDispatcher;
