'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _Shared = require('./Shared');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to perform requests to view server over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
var VirtuinTestViewClient = function VirtuinTestViewClient(stationName, testName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestViewClient);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var timeoutms = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5000;
      var numAttempts;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              numAttempts = Math.max(1, Math.ceil(timeoutms / _this.retryDelay));
              _context.next = 3;
              return (0, _Shared.openBroker)(brokerAddress, numAttempts, _this.retryDelay);

            case 3:
              _this.conn = _context.sent;
              _context.next = 6;
              return _this.conn.createChannel();

            case 6:
              _this.ch = _context.sent;

              _this.ch.assertExchange(_this.exName, 'direct', { durable: false });
              _this.isOpen = true;

            case 9:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (_this.isOpen) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt('return');

          case 2:
            _context2.next = 4;
            return _this.conn.close();

          case 4:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isOpen = false;

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.write = function (data) {
    var qName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

    if (!_this.isOpen) {
      return;
    }
    _this.ch.publish(_this.exName, qName || _this.qName, Buffer.from(data));
  };

  this.stationName = stationName;
  this.exName = stationName + '_view';
  this.qName = testName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
  this.retryDelay = 100;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Write data to view server.
 * @param {object} data - Serializable object to send to view server.
 * @param {string} qName - Name of queue to publish to.
 * @return {void}
 * @throws Error object
*/
;

exports.default = VirtuinTestViewClient;