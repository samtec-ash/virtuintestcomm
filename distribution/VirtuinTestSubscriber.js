'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _Shared = require('./Shared');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to subscribe to test's status updates over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
var VirtuinTestSubscriber = function VirtuinTestSubscriber(stationName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestSubscriber);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var timeoutms = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5000;
      var numAttempts;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              numAttempts = Math.max(1, Math.ceil(timeoutms / _this.retryDelay));
              _context.next = 3;
              return (0, _Shared.openBroker)(brokerAddress, numAttempts, _this.retryDelay);

            case 3:
              _this.conn = _context.sent;
              _context.next = 6;
              return _this.conn.createChannel();

            case 6:
              _this.ch = _context.sent;
              _context.next = 9;
              return _this.ch.assertExchange(_this.exName, 'fanout', { durable: false });

            case 9:
              _this.isOpen = true;

            case 10:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (_this.isOpen) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt('return');

          case 2:
            _context2.next = 4;
            return _this.conn.close();

          case 4:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isOpen = false;

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.subscribe = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(consumeCB) {
      var qName, q, consumeID;
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (_this.isOpen) {
                _context3.next = 2;
                break;
              }

              return _context3.abrupt('return', undefined);

            case 2:
              qName = '';
              _context3.next = 5;
              return _this.ch.assertQueue(qName, { exclusive: true });

            case 5:
              q = _context3.sent;

              _this.ch.bindQueue(q.queue, _this.exName, '');
              _context3.next = 9;
              return _this.ch.consume(q.queue, function (packet) {
                var jsonData = (0, _Shared.tryParseJSON)(packet.content) || {};
                consumeCB(null, jsonData);
              }, { noAck: true });

            case 9:
              consumeID = _context3.sent;
              return _context3.abrupt('return', consumeID.consumerTag);

            case 11:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this);
    }));

    return function (_x3) {
      return _ref3.apply(this, arguments);
    };
  }();

  this.unsubscribe = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(consumerTag) {
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              if (_this.isOpen) {
                _context4.next = 2;
                break;
              }

              return _context4.abrupt('return', undefined);

            case 2:
              return _context4.abrupt('return', _this.ch.cancel(consumerTag));

            case 3:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this);
    }));

    return function (_x4) {
      return _ref4.apply(this, arguments);
    };
  }();

  this.retryDelay = 100;
  this.exName = '' + stationName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Start subscribing to test status updates
 * @param {function} consumeCB - Called when status updates received
 * (error: ?Error, jsonData: Obj) => void
 * @return {string} consumerTag
 * @throws Error object
 */


/**
 * Stop subscribing to test status updates
 * @param {string} consumerTag - Queue tag returned from subscribing
 * @throws Error object
 */
;

exports.default = VirtuinTestSubscriber;