'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _Shared = require('./Shared');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to perform test requests (start/stop/status) over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
var VirtuinTestDispatcher = function VirtuinTestDispatcher(stationName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestDispatcher);

  this._generateUUID = function () {
    return Math.random().toString() + Math.random().toString() + Math.random().toString();
  };

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var timeoutms = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;
      var numAttempts, conn, ch;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              numAttempts = Math.max(1, Math.ceil(timeoutms / _this.retryDelay));
              _context.next = 3;
              return (0, _Shared.openBroker)(brokerAddress, numAttempts, _this.retryDelay);

            case 3:
              conn = _context.sent;
              _context.next = 6;
              return conn.createChannel();

            case 6:
              ch = _context.sent;

              ch.assertQueue(_this.testServerQName, { durable: true });
              _this.conn = conn;
              _this.ch = ch;
              _this.isOpen = true;

            case 11:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
    var consumerTags;
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (_this.isOpen) {
              _context3.next = 2;
              break;
            }

            return _context3.abrupt('return');

          case 2:
            consumerTags = Object.keys(_this.subscriptions);

            consumerTags.forEach(function () {
              var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(consumerTag) {
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return _this.unsubscribe(consumerTag);

                      case 2:
                      case 'end':
                        return _context2.stop();
                    }
                  }
                }, _callee2, _this);
              }));

              return function (_x3) {
                return _ref3.apply(this, arguments);
              };
            }());

            if (!(_this.conn != null)) {
              _context3.next = 7;
              break;
            }

            _context3.next = 7;
            return _this.conn.close();

          case 7:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.subscriptions = {};
            _this.isOpen = false;

          case 11:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, _this);
  }));

  this.subscribe = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(consumeCB) {
      var msTimeInterval = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5000;
      var ch, corrID, replyQ, statusInterval, consumeID;
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              if (!(!_this.isOpen || _this.ch == null)) {
                _context4.next = 2;
                break;
              }

              return _context4.abrupt('return', undefined);

            case 2:
              ch = _this.ch;
              corrID = _this._generateUUID();
              _context4.next = 6;
              return ch.assertQueue('', { durable: false, exclusive: true });

            case 6:
              replyQ = _context4.sent;
              statusInterval = setInterval(function () {
                var queueOptions = { correlationId: corrID, replyTo: replyQ.queue };
                var statusRequest = { command: 'STATUS' };
                var statusBuffer = Buffer.from(JSON.stringify(statusRequest));
                ch.sendToQueue(_this.testServerQName, statusBuffer, queueOptions);
              }, msTimeInterval);
              _context4.next = 10;
              return ch.consume(replyQ.queue, function (packet) {
                var jsonData = (0, _Shared.tryParseJSON)(packet.content) || {};
                consumeCB(null, jsonData);
              });

            case 10:
              consumeID = _context4.sent;

              _this.subscriptions[consumeID.consumerTag] = { statusInterval: statusInterval };
              return _context4.abrupt('return', consumeID.consumerTag);

            case 13:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this);
    }));

    return function (_x4) {
      return _ref4.apply(this, arguments);
    };
  }();

  this.unsubscribe = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(consumerTag) {
      var ch;
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              if (!(!_this.isOpen || _this.ch == null)) {
                _context5.next = 2;
                break;
              }

              return _context5.abrupt('return');

            case 2:
              ch = _this.ch;


              if (consumerTag in _this.subscriptions) {
                clearInterval(_this.subscriptions[consumerTag].statusInterval);
                ch.cancel(consumerTag);
                delete _this.subscriptions[consumerTag];
              }

            case 4:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, _this);
    }));

    return function (_x6) {
      return _ref5.apply(this, arguments);
    };
  }();

  this._sendCommand = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(payload) {
      var corrID = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
      var uuid, ch, payloadBuffer, replyQ, queueOptions;
      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              if (!(_this.ch == null)) {
                _context6.next = 2;
                break;
              }

              throw new Error('RabbitMQ channel is not open.');

            case 2:
              uuid = corrID || _this._generateUUID();
              ch = _this.ch;
              payloadBuffer = Buffer.from(JSON.stringify(payload));
              _context6.next = 7;
              return ch.assertQueue('', { durable: false, exclusive: true });

            case 7:
              replyQ = _context6.sent;
              queueOptions = { correlationId: uuid, replyTo: replyQ.queue };

              ch.sendToQueue(_this.testServerQName, payloadBuffer, queueOptions);

              return _context6.abrupt('return', Promise.race([new Promise(function (resolve, reject) {
                setTimeout(reject, 10000, new Error('Timeout occurred before receiving response from test server.'));
              }), new Promise(function (resolve) {
                ch.consume(replyQ.queue, function (packet) {
                  if (packet.properties.correlationId === uuid) {
                    ch.cancel(packet.fields.consumerTag);
                    ch.deleteQueue(replyQ.queue);
                    resolve(packet.content);
                  }
                }, { noAck: true });
              })]));

            case 11:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, _this);
    }));

    return function (_x7) {
      return _ref6.apply(this, arguments);
    };
  }();

  this.up = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(collection) {
      var fullReload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var runConfigs, response, rst;
      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              if (_this.isOpen) {
                _context7.next = 2;
                break;
              }

              throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');

            case 2:
              _context7.prev = 2;
              runConfigs = {
                collection: collection,
                fullReload: fullReload,
                command: 'UP'
              };
              _context7.next = 6;
              return _this._sendCommand(runConfigs, undefined);

            case 6:
              response = _context7.sent;
              rst = (0, _Shared.tryParseJSON)(response);
              return _context7.abrupt('return', rst);

            case 11:
              _context7.prev = 11;
              _context7.t0 = _context7['catch'](2);
              throw new Error('Failed bringing up test environment. Received error: ' + _context7.t0 + '.');

            case 14:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, _this, [[2, 11]]);
    }));

    return function (_x9) {
      return _ref7.apply(this, arguments);
    };
  }();

  this.down = function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8() {
      var rm = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var runConfigs, response, rst;
      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              if (_this.isOpen) {
                _context8.next = 2;
                break;
              }

              throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');

            case 2:
              _context8.prev = 2;
              runConfigs = {
                rm: rm,
                command: 'DOWN'
              };
              _context8.next = 6;
              return _this._sendCommand(runConfigs, undefined);

            case 6:
              response = _context8.sent;
              rst = (0, _Shared.tryParseJSON)(response);
              return _context8.abrupt('return', rst);

            case 11:
              _context8.prev = 11;
              _context8.t0 = _context8['catch'](2);
              throw new Error('Failed bringing down test environment. Received error: ' + _context8.t0 + '.');

            case 14:
            case 'end':
              return _context8.stop();
          }
        }
      }, _callee8, _this, [[2, 11]]);
    }));

    return function () {
      return _ref8.apply(this, arguments);
    };
  }();

  this.startTest = function () {
    var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(testConfigs) {
      var testUUID, runConfigs, response, rst;
      return _regenerator2.default.wrap(function _callee9$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              if (_this.isOpen) {
                _context9.next = 2;
                break;
              }

              throw Error('Failed starting test. Virtuin Test Dispatcher is not open.');

            case 2:
              _context9.prev = 2;
              testUUID = _this._generateUUID();
              runConfigs = (0, _extends3.default)({}, testConfigs, {
                command: 'START',
                testUUID: testUUID
              });
              _context9.next = 7;
              return _this._sendCommand(runConfigs, testUUID);

            case 7:
              response = _context9.sent;
              rst = (0, _Shared.tryParseJSON)(response);
              return _context9.abrupt('return', (0, _extends3.default)({}, rst, { testUUID: testUUID }));

            case 12:
              _context9.prev = 12;
              _context9.t0 = _context9['catch'](2);
              throw new Error('Failed starting test. Received error: ' + _context9.t0 + '.');

            case 15:
            case 'end':
              return _context9.stop();
          }
        }
      }, _callee9, _this, [[2, 12]]);
    }));

    return function (_x12) {
      return _ref9.apply(this, arguments);
    };
  }();

  this.stopTest = function () {
    var _ref10 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(testConfigs) {
      var runConfigs, response, rst;
      return _regenerator2.default.wrap(function _callee10$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              if (_this.isOpen) {
                _context10.next = 2;
                break;
              }

              throw Error('Failed stopping test. Virtuin Test Dispatcher is not open.');

            case 2:
              _context10.prev = 2;
              runConfigs = (0, _extends3.default)({}, testConfigs, {
                command: 'STOP'
              });
              _context10.next = 6;
              return _this._sendCommand(runConfigs, undefined);

            case 6:
              response = _context10.sent;
              rst = (0, _Shared.tryParseJSON)(response);
              return _context10.abrupt('return', rst);

            case 11:
              _context10.prev = 11;
              _context10.t0 = _context10['catch'](2);
              throw new Error('Failed stopping test. Received error: ' + _context10.t0 + '.');

            case 14:
            case 'end':
              return _context10.stop();
          }
        }
      }, _callee10, _this, [[2, 11]]);
    }));

    return function (_x13) {
      return _ref10.apply(this, arguments);
    };
  }();

  this.clearTest = function () {
    var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(testConfigs) {
      var runConfigs, response, rst;
      return _regenerator2.default.wrap(function _callee11$(_context11) {
        while (1) {
          switch (_context11.prev = _context11.next) {
            case 0:
              if (_this.isOpen) {
                _context11.next = 2;
                break;
              }

              throw Error('Failed clearing test. Virtuin Test Dispatcher is not open.');

            case 2:
              _context11.prev = 2;
              runConfigs = Object.assign({}, testConfigs, { command: 'CLEAR' });
              _context11.next = 6;
              return _this._sendCommand(runConfigs, undefined);

            case 6:
              response = _context11.sent;
              rst = (0, _Shared.tryParseJSON)(response);
              return _context11.abrupt('return', rst);

            case 11:
              _context11.prev = 11;
              _context11.t0 = _context11['catch'](2);
              throw new Error('Failed clearing test. Received error: ' + _context11.t0 + '.');

            case 14:
            case 'end':
              return _context11.stop();
          }
        }
      }, _callee11, _this, [[2, 11]]);
    }));

    return function (_x14) {
      return _ref11.apply(this, arguments);
    };
  }();

  this.getStatus = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12() {
    var packet, response, rst;
    return _regenerator2.default.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            if (_this.isOpen) {
              _context12.next = 2;
              break;
            }

            throw Error('Failed getting status. Virtuin Test Dispatcher is not open.');

          case 2:
            _context12.prev = 2;
            packet = Object.assign({}, { command: 'STATUS' });
            _context12.next = 6;
            return _this._sendCommand(packet, undefined);

          case 6:
            response = _context12.sent;
            rst = (0, _Shared.tryParseJSON)(response);
            return _context12.abrupt('return', rst);

          case 11:
            _context12.prev = 11;
            _context12.t0 = _context12['catch'](2);
            throw new Error('Failed getting status. Received error: ' + _context12.t0 + '.');

          case 14:
          case 'end':
            return _context12.stop();
        }
      }
    }, _callee12, _this, [[2, 11]]);
  }));

  this.stationName = stationName;
  this.testServerQName = '' + stationName;
  this.conn = undefined;
  this.ch = undefined;
  this.subscriptions = {};
  this.isOpen = false;
  this.retryDelay = 100;
}

/**
 * Create unique identifier.
 * @return {string} - unique identifier
 */


/**
* Open connection to RabbitMQ
* @async
* @param {string} brokerAddress - Address of broker
* @throws {object} Error object
* @return {void}
*/


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Start requesting & subscribing to test handler status updates
 * @async
 * @param {function} consumeCB - callback function (error: ?Error, jsonData: Obj) => void
 * @param {number} msTimeInterval - Time interval between querying status in milliseconds.
 * @return {string} consumerTag
 * @throws Error object
 */


/**
 * Stop requesting & subscribing to test handler status updates
 * @async
 * @param {string} consumerTag - Queue tag returned from subscribing
 * @throws Error object
 */


/**
 * Sends generic payload object to test handler
 * @async
 * @param {object} payload - Serializable payload object to send
 * @param {string} corrID - Optionally set identifier for reply queue
 * @returns {Promise}
 * @throws Error object
 */


/**
 * Request to bring up test environment
 * @async
 * @param {object} collection - Serializable test collection object
 * @returns {object} Result object
 * @throws Error object
 */


/**
 * Request to bring up test environment
 * @async
 * @param {object} collection - Serializable test collection object
 * @returns {object} Result object
 * @throws Error object
 */


/**
 * Request a test to start with given configs
 * @async
 * @param {object} testConfigs - Serializable test configs object to send
 * @returns {object} Result object
 * @throws Error object
 */


/**
 * Request a test to be killed if running
 * @async
 * @param {object} testConfigs - Test configs (need testName and testUUID at minimum)
 * @returns {object} result object
 * @throws Error object
 */


/**
 * Request to clear finished test
 * @async
 * @param {object} testConfigs - Test configs (logs boolean?)
 * @returns {object} result object
 * @throws Error object
 */


/**
 * Request status
 * @async
 * @returns {object} result object
 * @throws Error object
 */
;

exports.default = VirtuinTestDispatcher;