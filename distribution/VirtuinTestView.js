'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.VirtuinTestViewServer = exports.VirtuinTestViewClient = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _amqplib = require('amqplib');

var amqp = _interopRequireWildcard(_amqplib);

var _Shared = require('./Shared');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to perform requests to view server over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.3
 */

/**
 * @file Virtuin Test View consists of two classes: VirtuinTestViewClient and
 * VirtuinTestViewServer.
 * VirtuinTestViewClient shall be used from test view to send requests to test
 * view server via RabbitMQ.
 * VirtuinTestViewServer shall be used by test to listen for view requests via
 * RabbitMQ.
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 */

var VirtuinTestViewClient = function VirtuinTestViewClient(stationName, testName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestViewClient);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var host;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              host = 'amqp://' + brokerAddress;
              _context.next = 3;
              return amqp.connect(host);

            case 3:
              _this.conn = _context.sent;
              _context.next = 6;
              return _this.conn.createChannel();

            case 6:
              _this.ch = _context.sent;

              _this.ch.assertExchange(_this.exName, 'direct', { durable: false });
              _this.isOpen = true;

            case 9:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (_this.isOpen) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt('return');

          case 2:
            _context2.next = 4;
            return _this.conn.close();

          case 4:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isOpen = false;

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.write = function (data) {
    var qName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

    if (!_this.isOpen) {
      return;
    }
    _this.ch.publish(_this.exName, qName || _this.qName, Buffer.from(data));
  };

  this.stationName = stationName;
  this.exName = stationName + '_view';
  this.qName = testName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Write data to view server.
 * @param {object} data - Serializable object to send to view server.
 * @param {string} qName - Name of queue to publish to.
 * @return {void}
 * @throws Error object
*/
;

/**
 * @classdesc Used to listen for view requests over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.3
 */


var VirtuinTestViewServer = function VirtuinTestViewServer(stationName, testName) {
  var _this2 = this;

  (0, _classCallCheck3.default)(this, VirtuinTestViewServer);

  this.open = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var host, q;
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              host = 'amqp://' + brokerAddress;
              _context3.next = 3;
              return amqp.connect(host);

            case 3:
              _this2.conn = _context3.sent;
              _context3.next = 6;
              return _this2.conn.createChannel();

            case 6:
              _this2.ch = _context3.sent;

              _this2.ch.assertExchange(_this2.exName, 'direct', { durable: false });
              _context3.next = 10;
              return _this2.ch.assertQueue(_this2.qName, { exclusive: true });

            case 10:
              q = _context3.sent;

              _this2.ch.bindQueue(q.queue, _this2.exName, q.queue);
              _this2.ch.prefetch(1);
              _this2.isOpen = true;

            case 14:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this2);
    }));

    return function () {
      return _ref3.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4() {
    return _regenerator2.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            if (_this2.isOpen) {
              _context4.next = 2;
              break;
            }

            return _context4.abrupt('return');

          case 2:
            _context4.next = 4;
            return _this2.conn.close();

          case 4:
            _this2.conn = undefined;
            _this2.ch = undefined;
            _this2.isOpen = false;
            _this2.isListening = false;

          case 8:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, _this2);
  }));

  this.listen = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(consumeCB) {
      var consumeID;
      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              if (!(!_this2.isOpen || _this2.isListening)) {
                _context6.next = 2;
                break;
              }

              return _context6.abrupt('return');

            case 2:
              _context6.next = 4;
              return _this2.ch.consume(_this2.qName, function () {
                var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(packet) {
                  var jsonData, rstDict;
                  return _regenerator2.default.wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          _context5.prev = 0;
                          jsonData = (0, _Shared.tryParseJSON)(packet.content) || {};
                          // eslint-disable-next-line no-unused-vars

                          _context5.next = 4;
                          return consumeCB(null, jsonData);

                        case 4:
                          rstDict = _context5.sent;

                          // TODO: May want to reply with acknowledge packet when complete
                          // const replyQ = packet.properties.replyTo;
                          // const payloadBuffer = new Buffer(JSON.stringify(payload));
                          // const queueOptions = { correlationId: packet.properties.correlationId };
                          // this.ch.sendToQueue(replyQ, payloadBuffer, queueOptions);
                          _this2.ch.ack(packet);
                          _context5.next = 11;
                          break;

                        case 8:
                          _context5.prev = 8;
                          _context5.t0 = _context5['catch'](0);

                          consumeCB(new Error('Failed consuming packet ' + _context5.t0.message), null);

                        case 11:
                        case 'end':
                          return _context5.stop();
                      }
                    }
                  }, _callee5, _this2, [[0, 8]]);
                }));

                return function (_x5) {
                  return _ref6.apply(this, arguments);
                };
              }());

            case 4:
              consumeID = _context6.sent;

              _this2.isListening = true;
              return _context6.abrupt('return', consumeID.consumerTag);

            case 7:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, _this2);
    }));

    return function (_x4) {
      return _ref5.apply(this, arguments);
    };
  }();

  this.stationName = stationName;
  this.exName = stationName + '_view';
  this.qName = testName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
  this.isListening = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Listen for requests from view clients.
 * @param {function} consumeCB - callback function called whenever request is received
 * (error: ?Error, jsonData: Obj) => Obj
 * @return {string} consumerTag - Tag identifier used to explicitly stop listening
 * @throws Error object
*/
;

exports.VirtuinTestViewClient = VirtuinTestViewClient;
exports.VirtuinTestViewServer = VirtuinTestViewServer;