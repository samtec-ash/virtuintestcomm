'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _amqplib = require('amqplib');

var amqp = _interopRequireWildcard(_amqplib);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to handle test start/stop/status requests over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.8
 */
var VirtuinTestHandler = function VirtuinTestHandler(stationName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestHandler);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var host, conn, ch;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              host = 'amqp://' + brokerAddress;
              _context.next = 4;
              return amqp.connect(host);

            case 4:
              conn = _context.sent;
              _context.next = 7;
              return conn.createChannel();

            case 7:
              ch = _context.sent;
              _context.next = 10;
              return ch.assertQueue(_this.testServerQName, { durable: true });

            case 10:
              ch.prefetch(1);
              _this.conn = conn;
              _this.ch = ch;
              _this.isOpen = true;
              return _context.abrupt('return', true);

            case 17:
              _context.prev = 17;
              _context.t0 = _context['catch'](0);

              _this.isOpen = false;
              throw _context.t0;

            case 21:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this, [[0, 17]]);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!(_this.isOpen && _this.conn != null)) {
              _context2.next = 7;
              break;
            }

            _context2.next = 3;
            return _this.conn.close();

          case 3:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isListening = false;
            _this.isOpen = false;

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.listen = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(consumeCB) {
      var consumeID;
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              if (!(!_this.isOpen || _this.ch == null || _this.isListening)) {
                _context4.next = 2;
                break;
              }

              throw Error('Connection not open or already listening');

            case 2:
              _context4.next = 4;
              return _this.ch.consume(_this.testServerQName, function () {
                var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(packet) {
                  var jsonData, cmd, rstDict, replyQ, payload, payloadBuffer, queueOptions, ch;
                  return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.prev = 0;
                          jsonData = JSON.parse(packet.content);
                          cmd = jsonData.command;
                          _context3.next = 5;
                          return consumeCB(null, jsonData);

                        case 5:
                          rstDict = _context3.sent;
                          replyQ = packet.properties.replyTo;
                          payload = {
                            id: packet.properties.correlationId,
                            command: cmd,
                            error: undefined,
                            success: true
                          };

                          Object.assign(payload, rstDict);
                          payloadBuffer = Buffer.from(JSON.stringify(payload));
                          queueOptions = { correlationId: packet.properties.correlationId };

                          if (_this.ch != null) {
                            ch = _this.ch;

                            ch.sendToQueue(replyQ, payloadBuffer, queueOptions);
                            ch.ack(packet);
                          }
                          _context3.next = 17;
                          break;

                        case 14:
                          _context3.prev = 14;
                          _context3.t0 = _context3['catch'](0);

                          consumeCB(new Error('Failed consuming packet ' + _context3.t0.message), null);

                        case 17:
                        case 'end':
                          return _context3.stop();
                      }
                    }
                  }, _callee3, _this, [[0, 14]]);
                }));

                return function (_x3) {
                  return _ref4.apply(this, arguments);
                };
              }());

            case 4:
              consumeID = _context4.sent;

              _this.isListening = true;
              return _context4.abrupt('return', consumeID.consumerTag);

            case 7:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this);
    }));

    return function (_x2) {
      return _ref3.apply(this, arguments);
    };
  }();

  this.stationName = stationName;
  this.testServerQName = '' + stationName;
  this.conn = null;
  this.ch = null;
  this.isOpen = false;
  this.isListening = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Start listening for requests
 * @param {function} consumeCB - callback function called when request is received
 * (error: ?Error, jsonData: Obj) => Obj
 * @return {string} consumerTag - Tag identifier used to explicitly stop listening
 * @throws Error object
*/
;

exports.default = VirtuinTestHandler;