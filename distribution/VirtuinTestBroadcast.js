'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.VirtuinTestSubscriber = exports.VirtuinTestPublisher = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _amqplib = require('amqplib');

var amqp = _interopRequireWildcard(_amqplib);

var _Shared = require('./Shared');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @file VirtuinTestBroadcast consists of two classes: VirtuinTestPublisher and
 * VirtuinTestSubscriber.
 * VirtuinTestPublisher shall be used by a running test to update status via RabbitMQ.
 * VirtuinTestSubscriber shall be used by any process wanting to receive test status.
 * updates via RabbitMQ.
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 */

/**
 * @classdesc Used by active tests to publish status updates over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.3
 */
var VirtuinTestPublisher = function VirtuinTestPublisher(stationName, testName) {
  var _this = this;

  var testUUID = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
  (0, _classCallCheck3.default)(this, VirtuinTestPublisher);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var host;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              host = 'amqp://' + brokerAddress;
              _context.next = 3;
              return amqp.connect(host);

            case 3:
              _this.conn = _context.sent;
              _context.next = 6;
              return _this.conn.createChannel();

            case 6:
              _this.ch = _context.sent;
              _context.next = 9;
              return _this.ch.assertExchange(_this.exName, 'fanout', { durable: false });

            case 9:
              _this.isOpen = true;

            case 10:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (_this.isOpen) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt('return');

          case 2:
            _context2.next = 4;
            return _this.conn.close();

          case 4:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isOpen = false;

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this._getBasePayload = function () {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var error = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    return {
      version: _this.version,
      status: {
        testUUID: _this.testUUID,
        testName: _this.testName,
        state: _this.state,
        passed: _this.passed,
        progress: _this.progress,
        error: error,
        message: message
      }
    };
  };

  this._publishData = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(data) {
      var qName;
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              qName = ''; // Push to all queues in exchange

              _context3.next = 3;
              return _this.ch.publish(_this.exName, qName, Buffer.from(JSON.stringify(data)));

            case 3:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this);
    }));

    return function (_x5) {
      return _ref3.apply(this, arguments);
    };
  }();

  this.updateStatus = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
    var progress = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
    var passed = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;

    _this.state = state || _this.state;
    _this.progress = progress || _this.progress;
    _this.passed = passed !== undefined ? passed : _this.passed;
  };

  this.publish = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var error = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
      var results = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
      var customDict = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
      var data;
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              data = _this._getBasePayload(message, error);

              if (results) {
                data.results = results;
              }
              if (customDict) {
                data = Object.assign(data, customDict);
              }
              return _context4.abrupt('return', _this._publishData(data));

            case 4:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this);
    }));

    return function () {
      return _ref4.apply(this, arguments);
    };
  }();

  this.version = '0.9.3';
  this.exName = '' + stationName;
  this.testName = testName;
  this.testUUID = testUUID !== undefined ? testUUID : testName + '_' + new Date().toUTCString();
  this.state = 'STARTED';
  this.progress = 0;
  this.passed = undefined;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Create base payload object to be published
 * @param {string} message - General status message
 * @param {string} error - Error message
 * @return {object} - Payload object
 */


/**
 * Publishes payload object
 * @param {object} data - Payload object
 * @return {void}
 */


/**
 * Update test status payload
 * @param {string} state - State of test (STARTING, ..., FINISHED)
 * @param {number} progress - Progress of test [0-100]
 * @param {boolean} passed - If test passed/failed
 * @return {void}
 */


/**
 * Publish test status payload
 * @async
 * @param {string} message - General status message
 * @param {string} error - Error message
 * @param {object} results - Serializable Database results object
 * @param {object} customDict - Serializable object to merge into status payload
 * @return {void}
 * @throws {object} Error object
 */
;

/**
 * @classdesc Used to subscribe to test's status updates over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @version 0.9.3
 */


var VirtuinTestSubscriber = function VirtuinTestSubscriber(stationName) {
  var _this2 = this;

  (0, _classCallCheck3.default)(this, VirtuinTestSubscriber);

  this.open = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var host;
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              host = 'amqp://' + brokerAddress;
              _context5.next = 3;
              return amqp.connect(host);

            case 3:
              _this2.conn = _context5.sent;
              _context5.next = 6;
              return _this2.conn.createChannel();

            case 6:
              _this2.ch = _context5.sent;
              _context5.next = 9;
              return _this2.ch.assertExchange(_this2.exName, 'fanout', { durable: false });

            case 9:
              _this2.isOpen = true;

            case 10:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, _this2);
    }));

    return function () {
      return _ref5.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6() {
    return _regenerator2.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            if (_this2.isOpen) {
              _context6.next = 2;
              break;
            }

            return _context6.abrupt('return');

          case 2:
            _context6.next = 4;
            return _this2.conn.close();

          case 4:
            _this2.conn = undefined;
            _this2.ch = undefined;
            _this2.isOpen = false;

          case 7:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, _this2);
  }));

  this.subscribe = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(consumeCB) {
      var qName, q, consumeID;
      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              if (_this2.isOpen) {
                _context7.next = 2;
                break;
              }

              return _context7.abrupt('return', undefined);

            case 2:
              qName = '';
              _context7.next = 5;
              return _this2.ch.assertQueue(qName, { exclusive: true });

            case 5:
              q = _context7.sent;

              _this2.ch.bindQueue(q.queue, _this2.exName, '');
              _context7.next = 9;
              return _this2.ch.consume(q.queue, function (packet) {
                var jsonData = (0, _Shared.tryParseJSON)(packet.content) || {};
                consumeCB(null, jsonData);
              }, { noAck: true });

            case 9:
              consumeID = _context7.sent;
              return _context7.abrupt('return', consumeID.consumerTag);

            case 11:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, _this2);
    }));

    return function (_x14) {
      return _ref7.apply(this, arguments);
    };
  }();

  this.unsubscribe = function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(consumerTag) {
      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              if (_this2.isOpen) {
                _context8.next = 2;
                break;
              }

              return _context8.abrupt('return', undefined);

            case 2:
              return _context8.abrupt('return', _this2.ch.cancel(consumerTag));

            case 3:
            case 'end':
              return _context8.stop();
          }
        }
      }, _callee8, _this2);
    }));

    return function (_x15) {
      return _ref8.apply(this, arguments);
    };
  }();

  this.exName = '' + stationName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Start subscribing to test status updates
 * @param {function} consumeCB - Called when status updates received
 * (error: ?Error, jsonData: Obj) => void
 * @return {string} consumerTag
 * @throws Error object
 */


/**
 * Stop subscribing to test status updates
 * @param {string} consumerTag - Queue tag returned from subscribing
 * @throws Error object
 */
;

exports.VirtuinTestPublisher = VirtuinTestPublisher;
exports.VirtuinTestSubscriber = VirtuinTestSubscriber;