'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.openBroker = exports.tryParseJSON = undefined;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Open connection to RabbitMQ broker
 * @async
 * @param {string} brokerAddress - Address of broker
 * @param {number} maxAttempts - Number of attempts
 * @param {number} delayms - Millisecond delay
 * @throws {object} Error object
 * @return {void}
 */
var openBroker = function () {
  var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(brokerAddress, maxAttempts, delayms) {
    var _this = this;

    var openTimeout, hostAddress, conn, attempts;
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            openTimeout = function () {
              var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(host, delay) {
                return _regenerator2.default.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        return _context2.abrupt('return', new Promise(function (resolve) {
                          setTimeout((0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
                            var _conn;

                            return _regenerator2.default.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    _context.prev = 0;
                                    _context.next = 3;
                                    return amqp.connect(host);

                                  case 3:
                                    _conn = _context.sent;

                                    resolve(_conn);
                                    _context.next = 10;
                                    break;

                                  case 7:
                                    _context.prev = 7;
                                    _context.t0 = _context['catch'](0);

                                    resolve(null);

                                  case 10:
                                  case 'end':
                                    return _context.stop();
                                }
                              }
                            }, _callee, _this, [[0, 7]]);
                          })), delay);
                        }));

                      case 1:
                      case 'end':
                        return _context2.stop();
                    }
                  }
                }, _callee2, _this);
              }));

              return function openTimeout(_x4, _x5) {
                return _ref2.apply(this, arguments);
              };
            }();

            hostAddress = 'amqp://' + brokerAddress;
            conn = null;
            attempts = 0;

          case 4:
            if (!(attempts < maxAttempts)) {
              _context3.next = 13;
              break;
            }

            _context3.next = 7;
            return openTimeout(hostAddress, attempts > 0 ? delayms : 0);

          case 7:
            conn = _context3.sent;

            if (!(conn != null)) {
              _context3.next = 10;
              break;
            }

            return _context3.abrupt('return', conn);

          case 10:
            attempts += 1;
            _context3.next = 4;
            break;

          case 13:
            throw new Error('Failed connecting to broker. Ensure broker is running and address is correct');

          case 14:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function openBroker(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

/**
 * Parses json string into object. Doesn't throw with invalid json string.
 * @param  {string} jsonString - json string
 * @return {object|undefined} - json object or undefined if invalid json
 */


var _amqplib = require('amqplib');

var amqp = _interopRequireWildcard(_amqplib);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function tryParseJSON(jsonString) {
  try {
    var data = JSON.parse(jsonString);
    return data && (typeof data === 'undefined' ? 'undefined' : (0, _typeof3.default)(data)) === 'object' ? data : undefined;
  } catch (err) {
    return undefined;
  }
}

// eslint-disable-next-line import/prefer-default-export
exports.tryParseJSON = tryParseJSON;
exports.openBroker = openBroker;