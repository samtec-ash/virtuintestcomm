'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _Shared = require('./Shared');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @classdesc Used to listen for view requests over AMQP (via RabbitMQ).
 * @author Adam Page <adam.page@samtec.com>
 * @copyright Samtec 2017
 * @version 0.9.8
 */
var VirtuinTestViewServer = function VirtuinTestViewServer(stationName, testName) {
  var _this = this;

  (0, _classCallCheck3.default)(this, VirtuinTestViewServer);

  this.open = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var brokerAddress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'localhost';
      var timeoutms = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5000;
      var numAttempts, q;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              numAttempts = Math.max(1, Math.ceil(timeoutms / _this.retryDelay));
              _context.next = 3;
              return (0, _Shared.openBroker)(brokerAddress, numAttempts, _this.retryDelay);

            case 3:
              _this.conn = _context.sent;
              _context.next = 6;
              return _this.conn.createChannel();

            case 6:
              _this.ch = _context.sent;

              _this.ch.assertExchange(_this.exName, 'direct', { durable: false });
              _context.next = 10;
              return _this.ch.assertQueue(_this.qName, { exclusive: true });

            case 10:
              q = _context.sent;

              _this.ch.bindQueue(q.queue, _this.exName, q.queue);
              _this.ch.prefetch(1);
              _this.isOpen = true;

            case 14:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function () {
      return _ref.apply(this, arguments);
    };
  }();

  this.close = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (_this.isOpen) {
              _context2.next = 2;
              break;
            }

            return _context2.abrupt('return');

          case 2:
            _context2.next = 4;
            return _this.conn.close();

          case 4:
            _this.conn = undefined;
            _this.ch = undefined;
            _this.isOpen = false;
            _this.isListening = false;

          case 8:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.listen = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(consumeCB) {
      var consumeID;
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              if (!(!_this.isOpen || _this.isListening)) {
                _context4.next = 2;
                break;
              }

              return _context4.abrupt('return');

            case 2:
              _context4.next = 4;
              return _this.ch.consume(_this.qName, function () {
                var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(packet) {
                  var jsonData, rstDict;
                  return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.prev = 0;
                          jsonData = (0, _Shared.tryParseJSON)(packet.content) || {};
                          // eslint-disable-next-line no-unused-vars

                          _context3.next = 4;
                          return consumeCB(null, jsonData);

                        case 4:
                          rstDict = _context3.sent;

                          // TODO: May want to reply with acknowledge packet when complete
                          // const replyQ = packet.properties.replyTo;
                          // const payloadBuffer = new Buffer(JSON.stringify(payload));
                          // const queueOptions = { correlationId: packet.properties.correlationId };
                          // this.ch.sendToQueue(replyQ, payloadBuffer, queueOptions);
                          _this.ch.ack(packet);
                          _context3.next = 11;
                          break;

                        case 8:
                          _context3.prev = 8;
                          _context3.t0 = _context3['catch'](0);

                          consumeCB(new Error('Failed consuming packet ' + _context3.t0.message), null);

                        case 11:
                        case 'end':
                          return _context3.stop();
                      }
                    }
                  }, _callee3, _this, [[0, 8]]);
                }));

                return function (_x4) {
                  return _ref4.apply(this, arguments);
                };
              }());

            case 4:
              consumeID = _context4.sent;

              _this.isListening = true;
              return _context4.abrupt('return', consumeID.consumerTag);

            case 7:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, _this);
    }));

    return function (_x3) {
      return _ref3.apply(this, arguments);
    };
  }();

  this.retryDelay = 100;
  this.stationName = stationName;
  this.exName = stationName + '_view';
  this.qName = testName;
  this.conn = undefined;
  this.ch = undefined;
  this.isOpen = false;
  this.isListening = false;
}

/**
 * Open connection to RabbitMQ
 * @async
 * @param {string} brokerAddress - Address of broker
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Close connection to RabbitMQ
 * @async
 * @throws {object} Error object
 * @return {void}
 */


/**
 * Listen for requests from view clients.
 * @param {function} consumeCB - callback function called whenever request is received
 * (error: ?Error, jsonData: Obj) => Obj
 * @return {string} consumerTag - Tag identifier used to explicitly stop listening
 * @throws Error object
*/
;

exports.default = VirtuinTestViewServer;